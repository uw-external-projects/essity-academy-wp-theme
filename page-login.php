<?php
/* Template Name: EA: Login */

get_header('login');
?>

<div class="login-page">

    <div class="login-form">

        <div class="logo">
            <a href="<?php echo get_bloginfo('url'); ?>">
                <img src="<?php echo get_bloginfo('template_url'); ?>/img/ea_logo_main.svg" alt="Essity Academy">
            </a>
        </div>

        <form action="">

            <input type="text" class="form-input" name="name" placeholder="Nombre completo" required>

            <input type="email" class="form-input" name="email" placeholder="Correo electrónico" required>

            <input type="text" class="form-input" name="ticket" placeholder="Número de ticket" required>

            <label class="form-label" for="tos_check"><input type="checkbox" class="form-checkbox" name="tos" id="tos_check" required><span class="form-checkmark"></span>
                <a href="#">Acepto los Términos y Condiciones</a></label>

            <input type="submit" class="form-btn" value="Ingresar">

        </form>

        <?php
        if (have_posts()) :

            /* Start the Loop */
            while (have_posts()) :
                the_post();

                /*
                * Include the Post-Type-specific template for the content.
                * If you want to override this in a child theme, then include a file
                * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                */
                get_template_part('template-parts/content-page-login', get_post_type());

            endwhile;

        endif;
        ?>

    </div>

</div><!-- .login-page -->

<?php
get_footer('login');
